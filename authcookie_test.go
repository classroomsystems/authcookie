package authcookie

import (
	"net/http"
	"testing"
	"time"
)

func TestSignAndVerify(t *testing.T) {
	var ts = []struct {
		name        string
		value       string
		signedValue string
		expires     time.Time
		maxAge      int
		now         func() time.Time
		key         []byte
		verifyKeys  [][]byte
		signPanics  bool
		verifyFails bool
	}{
		{
			name:        "empty",
			value:       "",
			signedValue: "VZNJoEjlv8_VA9ZMYujLBGKNEcQO2bsAUByvP1eWCtTiL445",
		},
		{
			name:        "empty",
			value:       "",
			signedValue: "VZNJoEjlv8_VA9ZMYujLBGKNEcQO2bsAUByvP1eWCtTiL445",
			verifyKeys:  [][]byte{[]byte("something else"), []byte("1234567890")},
		},
		{
			name:        "empty",
			value:       "",
			signedValue: "VZNJoEjlv8_VA9ZMYujLBGKNEcQO2bsAUByvP1eWCtTiL445",
			verifyKeys:  [][]byte{[]byte("something else")},
			verifyFails: true,
		},
		{
			// Just as a sanity check that different keys make different signatures.
			name:        "emptykey",
			key:         []byte(""),
			value:       "",
			signedValue: "VZNJoCoAFYUXMephQ3dow4ppJjA7Q38Ofn5cy_G8N7-Izm0C",
		},
		{
			name:        "something",
			value:       "something|with|pipes",
			signedValue: "something|with|pipesVZNJoPAuPF1IOSGIVJrxow7Axm3_IYj3aNBXiBU4eRbmuT6Z",
		},
		{
			name:        "expires",
			expires:     in2015().Add(300 * time.Second),
			value:       "",
			signedValue: "VZMurK5ZE4I3qoJycjvessf1gIJCICisRG8nx45Hq5h7TibJ",
		},
		{
			name:        "max-age",
			maxAge:      300,
			value:       "",
			signedValue: "VZMurK5ZE4I3qoJycjvessf1gIJCICisRG8nx45Hq5h7TibJ",
		},
		{
			name:  "farFuture",
			now:   in2200,
			value: "",
			// The signed value from "empty" above, now expired.
			signedValue: "VZNJoEjlv8_VA9ZMYujLBGKNEcQO2bsAUByvP1eWCtTiL445",
			signPanics:  true,
			verifyFails: true,
		},
		{
			name:    "farFutureExpires",
			expires: in2200(),
			value:   "",
			// The signed value from "empty" above, but with the timestamp changed.
			// Take this opportunity to ensure that fails.
			signedValue: "ZZNJoEjlv8_VA9ZMYujLBGKNEcQO2bsAUByvP1eWCtTiL445",
			signPanics:  true,
			verifyFails: true,
		},
		{
			name:   "farFutureMaxAge",
			maxAge: 0xffffff * 300,
			value:  "",
			// Take this opportunity to ensure a missing signature fails.
			signedValue: "",
			signPanics:  true,
			verifyFails: true,
		},
	}
	oldNow := now
	for _, tc := range ts {
		if tc.now == nil {
			tc.now = in2015
		}
		if tc.key == nil {
			tc.key = []byte("1234567890")
		}
		now = tc.now
		k := NewKeySet(tc.key)
		c, panics := guardedSign(k, &http.Cookie{
			Name:    tc.name,
			Value:   tc.value,
			Expires: tc.expires,
			MaxAge:  tc.maxAge,
		})
		if panics && !tc.signPanics {
			t.Errorf("unexpected panic for %q", tc.name)
		}
		if !panics && tc.signPanics {
			t.Errorf("sign %q should have panicked", tc.name)
		}
		if !panics && !tc.signPanics && c.Value != tc.signedValue {
			t.Errorf("wrong signedValue for %q, got %q expecting %q",
				tc.name, c.Value, tc.signedValue)
		}
		req, _ := http.NewRequest("GET", "http://example.com/", nil)
		req.AddCookie(&http.Cookie{
			Name:  tc.name,
			Value: tc.signedValue,
		})
		if tc.verifyKeys != nil {
			k = NewKeySet(tc.verifyKeys[0], tc.verifyKeys[1:]...)
		}
		c, err := k.VerifiedCookie(req, tc.name)
		if !tc.verifyFails && err != nil {
			t.Errorf("unexpected verify error from %q: %v", tc.name, err)
		}
		if tc.verifyFails && err != ErrBadAuth {
			t.Errorf("wrong error from verify %q: got %v expecting ErrBadAuth", tc.name, err)
		}
		if !tc.verifyFails && err == nil && c.Value != tc.value {
			t.Errorf("wrong value for %q, got %q expecting %q",
				tc.name, c.Value, tc.value)
		}
	}
	now = oldNow
}

func in2015() time.Time { return time.Date(2015, 07, 01, 0, 0, 0, 0, time.UTC) }
func in2200() time.Time { return time.Date(2200, 07, 01, 0, 0, 0, 0, time.UTC) }

func guardedSign(k *KeySet, c *http.Cookie) (cookie *http.Cookie, panics bool) {
	defer func() {
		if recover() != nil {
			panics = true
		}
	}()
	cookie = k.Sign(c)
	return
}
