// Package authcookie authenticates HTTP cookie values using an HMAC signature.
//
// A signature is appended to cookie values to be sent to the client.
// When reading a cookie back from the client, the signature is verified and removed.
// Signatures include an expiration time, so cookie values are not valid eternally.
//
// Implementation details
//
// Callers should treat signed cookie values as opaque data
// and not rely on these details, which may change.
//
// The cookie expiration time is represented as
// an unsigned, 32-bit Unix time.
// Thus no signed value will be valid after 2106-02-07 06:28:15 UTC.
// The SHA256 hash algorithm is used for signing.
// Keys longer than 64 bytes are hashed to 32 bytes.
// The signature is base64-encoded and appended to the value, adding 48 bytes.
//
// If these details are changed,
// all reasonable care will be taken to ensure
// both API compatibility and the validity of previously-issued cookies.
package authcookie

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"net/http"
	"time"
)

// ErrBadAuth is returned by VerifiedCookie
// if the cookie value cannot be authenticated.
var ErrBadAuth = errors.New("cookie authentication failed")

// DefaultMaxAge is the default number of seconds
// a signed cookie value remains valid.
var DefaultMaxAge = 2 * 60 * 60

// RandomKey returns n bytes of cryptographically secure random data.
// It's a convenience for generating signing keys.
func RandomKey(n int) []byte {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		panic(err)
	}
	return b
}

var (
	decodedSig = 4 + sha256.Size
	encodedSig = base64.URLEncoding.EncodedLen(decodedSig)
)

// For testing.
var now = func() time.Time { return time.Now() }

// A KeySet holds signing and verification keys for authenticating cookie values.
type KeySet struct {
	keys [][]byte
}

// NewKeySet returns a new KeySet with the given keys.
// The key is used to sign and verify cookie values.
// Any oldKeys specified will be used to verify cookie values,
// but not to sign new values.
// This enables key rotation.
func NewKeySet(key []byte, oldKeys ...[]byte) *KeySet {
	a := &KeySet{make([][]byte, len(oldKeys)+1)}
	a.keys[0] = key
	for i := range oldKeys {
		a.keys[i+1] = oldKeys[i]
	}
	return a
}

// Sign appends an authentication code to c.Value and returns c.
//
// All authentication codes have an expiration time.
// The expiration time will be c.Expires if that's set.
// Otherwise if c.MaxAge is non-zero, the expiration time will be
// c.MaxAge seconds from now.
// If neither c.Expires nor c.MaxAge is set, the expiration time will be
// DefaultMaxAge seconds from now.
func (a *KeySet) Sign(c *http.Cookie) *http.Cookie {
	var expTime time.Time
	if !c.Expires.IsZero() {
		expTime = c.Expires
	} else {
		ma := DefaultMaxAge
		if c.MaxAge != 0 {
			ma = c.MaxAge
		}
		expTime = now().Add(time.Duration(ma) * time.Second)
	}
	expInt := expTime.Unix()
	if expInt < 0 || expInt > 0xffffffff {
		panic("expiration time out of range")
	}

	buf := make([]byte, len(c.Value)+decodedSig)
	copy(buf, c.Value)
	msg := buf[:len(c.Value)+4]
	encSrc := buf[len(c.Value):]

	out := make([]byte, len(c.Value)+encodedSig)
	copy(out, c.Value)
	encDst := out[len(c.Value):]

	encSrc[0] = byte(expInt >> 24 & 0xff)
	encSrc[1] = byte(expInt >> 16 & 0xff)
	encSrc[2] = byte(expInt >> 8 & 0xff)
	encSrc[3] = byte(expInt & 0xff)

	m := hmac.New(sha256.New, a.keys[0])
	m.Write(msg)
	m.Sum(encSrc[4:4]) // length 0 so Sum can append
	base64.URLEncoding.Encode(encDst, encSrc)
	c.Value = string(out)
	return c
}

// VerifiedCookie retrieves the named cookie from r and verifies its signature.
// If the cookie is not found, http.ErrNoCookie is returned.
// If the cookie's signature is invalid, ErrBadAuth is returned.
// If the returned error is nil, the cookie's Value is valid.
// The returned Cookie does not contain the signature.
func (a *KeySet) VerifiedCookie(r *http.Request, name string) (*http.Cookie, error) {
	c, err := r.Cookie(name)
	if err != nil {
		return c, err
	}
	if len(c.Value) < encodedSig {
		return c, ErrBadAuth
	}

	buf := make([]byte, len(c.Value)+decodedSig)
	val := buf[encodedSig : len(buf)-decodedSig]
	msg := buf[encodedSig : len(buf)-decodedSig+4]
	decSrc := buf[:encodedSig]
	decDst := buf[len(buf)-decodedSig:]
	mac := buf[len(buf)-decodedSig+4:]
	copy(val, c.Value)
	copy(decSrc, c.Value[len(c.Value)-encodedSig:])

	n, err := base64.URLEncoding.Decode(decDst, decSrc)
	if err != nil || n != len(decDst) {
		return c, ErrBadAuth
	}

	expTime := time.Unix(int64(decDst[0])<<24|int64(decDst[1])<<16|int64(decDst[2])<<8|int64(decDst[3]), 0)
	if now().After(expTime) {
		return c, ErrBadAuth
	}

	c.Value = c.Value[:len(c.Value)-encodedSig]

	var macOut []byte
	for _, k := range a.keys {
		macOut = macOut[:0]
		m := hmac.New(sha256.New, k)
		m.Write(msg)
		macOut := m.Sum(macOut)
		if hmac.Equal(macOut, mac) {
			return c, nil
		}
	}
	return c, ErrBadAuth
}
